# Quarterly Goals

This project tracks progress towards quarterly goals.

Milestones relate to the goals themselves, while the attached issues attached are for the OKRs associated with them.
